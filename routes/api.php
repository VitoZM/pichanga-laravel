<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\BalanceController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\PermissionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// @TODO: Auth
Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', [AuthController::class, 'me']);

});

Route::post('/log/create', [LogController::class, 'create']);
Route::post('/log/read', [LogController::class, 'read']);
Route::post('/log/update', [LogController::class, 'update']);
Route::post('/log/dashboard', [LogController::class, 'dashboard']);

Route::post('/user/create', [UserController::class, 'create']);
Route::post('/user/read', [UserController::class, 'read']);
Route::post('/user/delete', [UserController::class, 'delete']);
Route::post('/user/findByUserName', [UserController::class, 'findByUserName']);

Route::post('/profile/read', [ProfileController::class, 'read']);
Route::post('/profile/update', [ProfileController::class, 'update']);
Route::post('/profile/alreadyUsedCi', [ProfileController::class, 'alreadyUsedCi']);
Route::post('/profile/alreadyUsedCiOnUpdate', [ProfileController::class, 'alreadyUsedCiOnUpdate']);
Route::post('/profile/alreadyUsedEmail', [ProfileController::class, 'alreadyUsedEmail']);
Route::post('/profile/alreadyUsedEmailOnUpdate', [ProfileController::class, 'alreadyUsedEmailOnUpdate']);
Route::post('/profile/findById', [ProfileController::class, 'findById']);
Route::post('/profile/getProfileAndBalance', [ProfileController::class, 'getProfileAndBalance']);

Route::post('/task/create', [TaskController::class, 'create']);
Route::post('/task/read', [TaskController::class, 'read']);
Route::post('/task/update', [TaskController::class, 'update']);
Route::post('/task/delete', [TaskController::class, 'delete']);
Route::post('/task/findById', [TaskController::class, 'findById']);
Route::post('/task/getTitleAndPayments', [TaskController::class, 'getTitleAndPayments']);
Route::post('/task/finish', [TaskController::class, 'finish']);

Route::post('/balance/create', [BalanceController::class, 'create']);
Route::post('/balance/read', [BalanceController::class, 'read']);
Route::post('/balance/update', [BalanceController::class, 'update']);

Route::post('/payment/create', [PaymentController::class, 'create']);
Route::post('/payment/deletePaymentsExceptFirst', [PaymentController::class, 'deletePaymentsExceptFirst']);

Route::post('/message/create', [MessageController::class, 'create']);
Route::post('/message/read', [MessageController::class, 'read']);
Route::post('/message/update', [MessageController::class, 'update']);
Route::post('/message/delete', [MessageController::class, 'delete']);
Route::post('/message/findById', [MessageController::class, 'findById']);

Route::post('/client/create', [ClientController::class, 'create']);
Route::post('/client/read', [ClientController::class, 'read']);
Route::post('/client/update', [ClientController::class, 'update']);
Route::post('/client/delete', [ClientController::class, 'delete']);

Route::post('/permission/read', [PermissionController::class, 'read']);

