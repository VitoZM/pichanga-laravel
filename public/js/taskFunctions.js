function getTaskTypeHtml(type) {
    let html = "";
    switch (type) {
        case "1":
            html = '<label class="badge badge-light-primary">Tarea</label>';
            break;
        case "2":
            html = '<label class="badge badge-light-danger">Examen</label>';
            break;
    }
    return html;
}

function getTaskStatusHtml(status) {
    let html = "";
    switch (status) {
        case "0":
            html = '<label class="badge badge-light-danger">Pendiente</label>';
            break;
        case "1":
            html = '<label class="badge badge-light-success">Terminado</label>';
            break;
        case "2":
            html =
                '<label class="badge badge-light-warning">En Proceso</label>';
            break;
    }
    return html;
}

function getTaskScoreHtml(score) {
    let html = "";
    score = parseFloat(score);
    if (score == 5)
        html = `<label class="badge text-success">
                    <i class="feather icon-star-on"></i>
                    ${score}
                </label>`;
    else {
        if (score >= 3)
            html = `<label class="badge text-primary">
                        <i class="feather icon-star-on"></i>
                        ${score}
                    </label>`;
        else
            html = `<label class="badge text-danger">
                        <i class="feather icon-star-on"></i>
                        ${score}
                    </label>`;
    }
    return html;
}

function fillTaskPayments(payments) {
    let html = "";
    payments.forEach((payment) => {
        let date = dateWithNoSeconds(payment.created_at);
        html += `<tr>
					<td>${payment.paymentId}</td>
					<td>${date}</td>
					<td>
                        <p class="text-muted font-weight-bold text-success">${payment.amount} Bs.</p>
                    </td>
                </tr>`;
    });
    $("#paymentsTable").html(html);
}

function setClientSelect() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/client/read";
    $.post({
        url: url,
        headers: headers,
        statusCode: {
            200: (response) => {
                setClientSelectHtml(response.clients);
            },
            400: (response) => {
                console.log(response);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function setClientSelectHtml(clients) {
    let html = `<option value=""hidden></option>`;
    let first = true;
    clients.forEach((client) => {
        clientsArray[client.clientId] = client.phone;
        if (first) {
            html += `<option value="${client.clientId}" selected>${client.name} (${client.clientId})</option>`;
            $("#phone").val(client.phone);
            first = false;
        } else
            html += `<option value="${client.clientId}">${client.name} (${client.clientId})</option>`;
    });
    $("#clientId").html(html);
}

$("#newClientBtn").on("click", function (e) {
    e.preventDefault();
    newClient = true;
    $("#newClientForm").removeClass("d-none");
    $("#newClientForm").fadeIn();
    $("#oldClientBtnForm").removeClass("d-none");
    $("#oldClientBtnForm").fadeIn();
    $("#oldClientForm").fadeOut();
    $("#newClientBtnForm").fadeOut();
    $("#phone").val("");
    $("#phone").removeAttr("readonly");
});

$("#oldClientBtn").on("click", function (e) {
    e.preventDefault();
    newClient = false;
    $("#newClientForm").fadeOut();
    $("#newClientBtnForm").fadeOut();
    $("#oldClientForm").fadeIn();
    $("#newClientBtnForm").fadeIn();
    $("#oldClientBtnForm").fadeOut();
    let clientId = $("#clientId").val();
    $("#phone").val(clientsArray[clientId]);
    $("#phone").attr("readonly", "true");
});

$("#clientId").change(function () {
    let clientId = $("#clientId").val();
    $("#phone").val(clientsArray[clientId]);
});
