var knowledgesOnView = 0;
var knowledgeId = 0;
var actualUserId;

isLogged("read-users");
read();

function read() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/profile/read";
    $.post({
        url: url,
        headers: headers,
        statusCode: {
            200: (response) => {
                fillUsers(response.profiles);
                $("#loading").fadeOut(500);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function fillUsers(profiles) {
    $("#table").html("");
    profiles.forEach((profile) => {
        let name = profile.name + " " + profile.lastName;
        let ci = profile.ci + profile.issued;
        let userId = profile.userId;
        let city = profile.city;
        let phone = profile.phone;
        let email = profile.email;
        let photo = profile.photo;
        let bankAccount = profile.bankAccount;
        let bank = profile.bank;
        let knowledges = profile.knowledges;
        let knowledgesTag = getKnowledgesHtml(knowledges);
        let score = parseFloat(profile.score).toFixed(1);
        let balance = getBalanceTag(profile.balance);

        let html = `<tr id="row${userId}">
                        <td>
                            <div class="d-inline-block align-middle">
                                <img src="assets/images/users/${photo}" alt="user image"
                                    class="img-radius wid-40 align-top m-r-15">
                                <div class="d-inline-block">
                                    <h6>${name} <span style="color:#FF4F5A;">(${userId})</span></h6>
                                    <p class="text-muted m-b-0">${city}</p>
                                </div>
                            </div>
                        </td>
                        <td><a href="https://wa.me/591${phone}" target="_blank">${phone}</a></td>
                        <td>${knowledgesTag}</td>
                        <td><i class="feather icon-star-on text-warning"></i>
                            ${score}
                        </td>
                        <td>${balance}</td>
                        <td>
                            <a class="success p-0" type="button" title="Editar Auxiliar"
                                onclick="showEditModal(${userId});">
                                <i class="feather icon-edit"></i>
                            </a>
                            <a class="success p-0" type="button" data-toggle="modal" title="Eliminar Auxiliar"
                                onclick="showDeleteModal(${userId});">
                                <i class="feather icon-x"></i>
                            </a>
                            <a class="success p-0" type="button" title="Mostrar Saldo Del Auxiliar"
                                onclick="showBalanceModal(${userId});">
                                <i class="feather icon-credit-card"></i>
                            </a>
                            <a class="success p-0" type="button" data-toggle="modal" title="Perfil Del Auxiliar"
                                onclick="showUserProfile(${userId});">
                                <i class="feather icon-eye"></i>
                            </a>
                        </td>
                    </tr>`;
        $("#table").append(html);
    });
}

$("#updateBtn").on("click", function (e) {
    e.preventDefault();
    if (error()) return;
    update();
});

function error() {
    let anyError = false;
    let errors = Array();
    errors.push(withoutKnowledges());
    errors.push(textBoxIsEmpty("bankAccount"));
    errors.push(textBoxIsEmpty("email"));
    errors.push(
        uniqueOnUpdateAjaxValidation(
            "api/profile/alreadyUsedEmailOnUpdate",
            "email",
            actualUserId
        )
    );
    errors.push(textBoxIsEmpty("phone"));
    errors.push(textBoxIsEmpty("city"));
    errors.push(textBoxIsEmpty("ci"));
    errors.push(
        uniqueOnUpdateAjaxValidation(
            "api/profile/alreadyUsedCiOnUpdate",
            "ci",
            actualUserId
        )
    );
    errors.push(textBoxIsEmpty("lastName"));
    errors.push(textBoxIsEmpty("name"));
    errors.forEach((error) => {
        if (error) anyError = true;
    });
    return anyError;
}

$("#deleteBtn").click(function () {
    let userId = actualUserId;
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/user/delete";
    let data = { userId: userId };
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                $("#closeDeleteModal").click();
                Swal.fire({
                    type: "success",
                    title: "CORRECTO",
                    text: "¡Auxiliar eliminado exitosamente!",
                    animation: true,
                }).then(() => {
                    $("#row" + userId).fadeOut();
                });
            },
            400: (response) => {
                console.log(response);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
});

function update() {
    let token = sessionStorage.token
        ? sessionStorage.token
        : localStorage.token;
    let url = "api/profile/update";
    let data = getJsonForm("form", [
        "ci",
        "issued",
        "phone",
        "bankAccount",
        "bank",
    ]);
    let headers = {
        Authorization: "Bearer " + token,
    };
    data.email = data.email.toLowerCase();
    data.knowledges = JSON.stringify(getKnowledges());
    data.userId = actualUserId;
    $.post({
        url: url,
        data: data,
        headers: headers,
        statusCode: {
            200: (response) => {
                $("#closeEditModal").click();
                read();
                Swal.fire({
                    type: "success",
                    title: "CORRECTO",
                    text: "¡Auxiliar añadido exitosamente!",
                    animation: true,
                });
            },
            500: (response) => {},
        },
    });
}

function getBalanceTag(balance) {
    let html = "";
    if (balance >= 0)
        html = `<label class="badge badge-light-success">${balance} Bs</label>`;
    else html = `<label class="badge badge-light-danger">${balance} Bs</label>`;
    return html;
}

function showBalanceModal(userId) {
    actualUserId = userId;
    let token = sessionStorage.token
        ? sessionStorage.token
        : localStorage.token;
    let url = "api/profile/getProfileAndBalance";
    let data = { userId: userId };
    let headers = {
        Authorization: "Bearer " + token,
    };
    $.post({
        url: url,
        data: data,
        headers: headers,
        statusCode: {
            200: (response) => {
                let profile = response.profile;
                let fullName = `${profile.name} ${profile.lastName} (${userId})`;
                let balances = response.balances;
                $(".userBalanceName").html(fullName);
                let html = "";
                balances.forEach((balance) => {
                    if (balance.amount > 0)
                        html += '<tr class="p-3 mb-2 bg-success text-white">';
                    else html += '<tr class="p-3 mb-2 bg-danger text-white">';
                    html += `
                        <td>${balance.description}</td>
                        <td>${balance.amount} Bs.</td>
                    </tr>`;
                });
                $("#balances").html(html);
                let textColor = "";
                let lastBalance =
                    balances[0] == undefined ? 0 : balances[0].amount;
                if (lastBalance > 0) textColor = "text-success";
                else textColor = "text-danger";
                $("#lastBalance")
                    .html(`<label class="elevacion font-weight-bold text-dark">Último Movimiento</label>
                                 <p class="text-muted font-weight-bold ${textColor}">${lastBalance} Bs.</p>`);
                if (profile.balance > 0) textColor = "text-success";
                else textColor = "text-danger";
                $("#actualBalance")
                    .html(`<label class="elevacion font-weight-bold text-dark">Saldo Actual</label>
                            <p class="text-muted font-weight-bold ${textColor}">${parseFloat(
                    profile.balance
                ).toFixed(2)} Bs.</p>`);
                $("#userBalanceModal").modal("show");
            },
            500: (response) => {},
        },
    });
}

function showUserProfile(userId) {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/profile/findById";
    let data = { userId: userId };
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                let profile = response.profile;
                $("#profilePhoto").attr(
                    "src",
                    "assets/images/users/" + profile.photo
                );
                $("#profileUserId").html(userId);
                $("#profileName").html(profile.name);
                $("#profileLastName").html(profile.lastName);
                $("#profileCi").html(profile.ci + " " + profile.issued);
                $("#profileCity").html(profile.city);
                $("#profilePhone").html(
                    `<a href="https://wa.me/591${profile.phone}" target="_blank">${profile.phone}</a>`
                );
                $("#profileEmail").html(profile.email);
                $("#profileBankAccount").html(profile.bankAccount);
                $("#profileBank").html(profile.bank);
                $("#profileBalance").html(
                    parseFloat(profile.balance).toFixed(2)
                );
                $("#profileScore").html(parseFloat(profile.score).toFixed(1));
                let knowledgesHtml = getProfileKnowledges(profile.knowledges);
                $("#profileKnowledges").html(knowledgesHtml);

                fillUserTaks(response.tasks);

                $("#userProfileModal").modal("show");
            },
            400: (response) => {
                console.log(response);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function fillUserTaks(tasks) {
    let html = "";
    tasks.forEach((task) => {
        let taskType = getTaskTypeHtml(task.type);
        let taskStatus = getTaskStatusHtml(task.status);
        let taskScore = getTaskScoreHtml(task.score);
        html += `<tr>
					<td>${task.subject}</td>
					<td>${task.client}</td>
					<td>${taskType}</td>
					<td>${task.finalCost}</td>
					<td>${taskStatus}</td>
					<td>${taskScore}</td>
                </tr>`;
    });
    if (html != "") $("#tasksTable").html(html);
    else
        $("#tasksTable").html(
            '<tr><td colspan="6"><b>SIN TAREAS REALIZADAS</b></td></tr>'
        );
}

function getProfileKnowledges(knowledges) {
    knowledges = JSON.parse(knowledges);
    let html = "";
    let first = true;
    knowledges.forEach((knowledge) => {
        if (!first) html += ", ";
        else first = false;
        html += knowledge;
    });
    return html;
}

function getPerfilConocimientos(idAuxiliar) {
    let conocimientos = JSON.parse(auxiliares[idAuxiliar].conocimientos);
    let htmlConocimientos = "";
    conocimientos.forEach((conocimiento) => {
        if (htmlConocimientos != "") htmlConocimientos += ", ";
        htmlConocimientos += conocimiento;
    });
    return htmlConocimientos;
}

function showEditModal(userId) {
    clean();
    actualUserId = userId;
    knowledgesOnView = 0;
    knowledgeId = 0;
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/profile/findById";
    let data = { userId: userId };
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                fillEditForm(response.profile);
                $("#editModal").modal("show");
            },
            400: (response) => {
                console.log(response);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function fillEditForm(profile) {
    //con este for llenamos todo un form al toque mi rey
    for (var key in profile)
        if (key != "knowledges") $("#" + key).val(profile[key]);
    let knowledges = JSON.parse(profile.knowledges);
    $("#knowledges").html("");
    knowledges.forEach((knowledge) => {
        addKnowledge(event, knowledge);
    });
}

function showDeleteModal(userId) {
    actualUserId = userId;
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let data = { userId: userId };
    let url = "api/profile/findById";
    $.post({
        url: url,
        data: data,
        headers: headers,
        statusCode: {
            200: (response) => {
                let profile = response.profile;
                let html = `<p>ID.: <span class="font-weight-bold">${userId}
                                    </span>
                            <br>Estas seguro que quieres eliminar al auxiliar
                            <span class="font-weight-bold">
                                ${profile.name} ${profile.lastName}
                            </span>?</p>`;
                $("#eliminarModalBody").html(html);
                $("#eliminarAuxiliarModal").modal("show");
            },
            201: (response) => {
                console.log(response);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function getKnowledgesHtml(knowledges) {
    let html = "";
    knowledges = JSON.parse(knowledges);
    knowledges.forEach((knowledge) => {
        if (html != "") html += "<br>";
        html += knowledge;
    });
    return html;
}

function addKnowledge(event, knowledge) {
    event.preventDefault();
    knowledgesOnView++;
    knowledgeId++;
    let html = `<div data-role="appendRow" id="knowledgeDiv${knowledgeId}">
                    <div class="form-inline form-group">
                        <input input type="text"
                            class="form-control mb-2 mr-sm-2 col-12 col-sm-9 col-md-8 col-lg-9"
                            id="knowledge${knowledgeId}" placeholder="Area de conocimiento"
                            value="${knowledge == undefined ? "" : knowledge}"
                            >
                        <!-- file upload ends-->
                        <div class="mx-auto">
                            <button
                                class="btn btn-sm btn-outline-danger rounded-circle mr-2 mb-2 botoncitoX"
                                onclick="removeKnowledge(event,${knowledgeId})">
                                <i class="feather icon-x"></i>
                            </button>
                            <button
                                class="btn btn-sm btn-outline-primary rounded-circle mb-2 botoncitoMas"
                                onclick="addKnowledge(event);">
                                <i class="feather icon-plus"></i>
                            </button>
                        </div>
                        <p class="text-danger small m-0 d-none"
                            id="knowledge${knowledgeId}ErrorMessage"><i
                                class="feather icon-alert-circle"></i> Debe introducir al menos un conocimiento
                        </p>
                    </div> <!-- /div.form-inline -->
                </div>`;
    $("#knowledges").append(html);
}

$("#createBalanceBtn").click(function (event) {
    event.preventDefault();
    if (errorOnBalance()) return;
    createBalance();
});

function errorOnBalance() {
    let anyError = false;
    let errors = Array();
    errors.push(textBoxIsEmpty("description"));
    errors.push(textBoxNumericIsEmpty("amount"));
    errors.forEach((error) => {
        if (error) anyError = true;
    });
    return anyError;
}

function createBalance() {
    let userId = actualUserId;
    let description = $("#description").val();
    let amount = $("#amount").val();
    let url = "api/balance/create";
    let data = { userId: userId, description: description, amount: amount };
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    $.post({
        url: url,
        data: data,
        headers: headers,
        statusCode: {
            200: (response) => {
                $("#userBalanceModal").modal("hide");
                $("#newBalance").modal("hide");
                Swal.fire({
                    type: "success",
                    title: "CORRECTO",
                    text: "¡Saldo insertado exitosamente!",
                    animation: true,
                }).then((response) => {
                    showBalanceModal(userId);
                });
                read();
                clean();
                $("#description").val("");
                $("#amount").val("");
            },
            400: (response) => {
                console.log(response);
            },
        },
    });
}

function removeConocimiento(e, idConocimiento) {
    e.preventDefault();
    if (contadorConocimientosEnVista == 1) return;
    $("#grupoConocimiento" + idConocimiento).fadeOut();
    $("#conocimiento" + idConocimiento).val("");
    contadorConocimientosEnVista--;
}
