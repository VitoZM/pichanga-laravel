isLogged("read-messages");
read();
var actualMessageId;

function read() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/message/read";
    $.post({
        url: url,
        headers: headers,
        success: (response) => {
            fillMessages(response.messages);
            $("#loading").fadeOut(500);
            fillMessageButtonsByAjax();
        },
    });
}

function fillMessages(messages) {
    $("#table").html("");
    messages.forEach((message) => {
        let messageId = message.messageId;
        let title = message.title;
        let description = message.description;

        let html = `<tr id="row-${messageId}">
                        <td>${messageId}</td>
                        <td>
                            <a href="#"
                                    class="btn btn-primary"
                                    onclick="copyMessage(event,${messageId});"
                                >
                                    ${title}
                                </a>
                        </td>
                        <td>${description}</td>
                        <td>
                            <a class="success p-0" type="button" title="Editar Auxiliar"
                                onclick="showEditModal(${messageId});">
                                <i class="feather icon-edit"></i>
                            </a>
                            <a class="success p-0" type="button" data-toggle="modal" title="Eliminar Auxiliar"
                                onclick="showDeleteModal(${messageId});">
                                <i class="feather icon-x"></i>
                            </a>
                        </td>
                    </tr>`;
        $("#table").append(html);
    });
}

function showEditModal(messageId) {
    clean();
    actualMessageId = messageId;
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/message/findById";
    let data = { messageId: messageId };
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                fillEditForm(response.message);
                $("#editModal").modal("show");
            },
            400: (response) => {
                console.log(response);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

$("#updateBtn").on("click", function (e) {
    e.preventDefault();
    if (error()) return;
    update();
});

function error() {
    let anyError = false;
    let errors = Array();
    errors.push(textBoxIsEmpty("title"));
    errors.push(textBoxIsEmpty("description"));
    errors.forEach((error) => {
        if (error) anyError = true;
    });
    return anyError;
}

function update() {
    let token = sessionStorage.token
        ? sessionStorage.token
        : localStorage.token;
    let url = "api/message/update";
    let messageId = actualMessageId;
    let title = $("#title").val();
    let description = $("#description").val();
    let data = {
        messageId: messageId,
        title: title,
        description: description,
    };
    let headers = {
        Authorization: "Bearer " + token,
    };
    $.post({
        url: url,
        data: data,
        headers: headers,
        statusCode: {
            200: (response) => {
                $("#editModal").modal("hide");
                Swal.fire({
                    type: "success",
                    title: "CORRECTO",
                    text: "¡Mensaje editado exitosamente!",
                    animation: true,
                }).then(() => {
                    clean();
                });
                read();
                fillMessageButtonsByAjax();
            },
            500: (response) => {},
        },
    });
}

function fillEditForm(message) {
    $("#editModalCenterTitle").html(`Editar Mensaje (${message.messageId})`);
    for (var key in message) $("#" + key).val(message[key]);
}

function showDeleteModal(messageId) {
    actualMessageId = messageId;
    $("#deleteModalCenterTitle").html(`Eliminar mensaje (${messageId})`);
    $("#deleteMessageModal").modal("show");
}

$("#deleteBtn").on("click", function (event) {
    event.preventDefault();
    let token = sessionStorage.token
        ? sessionStorage.token
        : localStorage.token;
    let url = "api/message/delete";
    let messageId = actualMessageId;
    let data = { messageId: messageId };
    let headers = {
        Authorization: "Bearer " + token,
    };
    $.post({
        url: url,
        data: data,
        headers: headers,
        statusCode: {
            200: (response) => {
                $("#deleteMessageModal").modal("hide");
                Swal.fire({
                    type: "success",
                    title: "CORRECTO",
                    text: "¡Mensaje eliminado exitosamente!",
                    animation: true,
                }).then(() => {});
                read();
                fillMessageButtonsByAjax();
            },
            500: (response) => {},
        },
    });
});
