messagesArray = [];

function fillMessageButtonsByAjax() {
    //función que pide los botones y los llena
    let token = sessionStorage.token
        ? sessionStorage.token
        : localStorage.token;
    let url = "api/message/read";
    let headers = {
        Authorization: "Bearer " + token,
    };
    $.post({
        url: url,
        headers: headers,
        statusCode: {
            200: (response) => {
                messages = response.messages;
                fillMessageButtons(messages);
            },
            500: (response) => {},
        },
    });
}

function fillMessageButtons(messages) {
    //función que llena los botones
    $("#messages").html("");
    messages.forEach((message) => {
        messagesArray[message.messageId] = message.description;
        let html = `<a href="#"
                                    class="btn btn-primary"
                                    onclick="copyMessage(event,${message.messageId});"
                                >
                                    ${message.title}
                                </a>`;
        $("#messages").append(html);
    });
}

function copyMessage(event, id) {
    event.preventDefault();
    var aux = document.createElement("input");
    aux.setAttribute("value", messagesArray[id]);
    document.body.appendChild(aux);
    aux.select();
    document.execCommand("copy");
    document.body.removeChild(aux);
    Swal.fire({
        type: "success",
        title: "COPIADO",
        text: messagesArray[id],
        animation: true,
    }).then(() => {});
}
