isLogged("read-tasks");
read();
setReceptionDate();
var clientsArray = [];
var newClient = false;
var actualTask;
var actualTaskId;
var prevBoard;
var acceptBtn = false;

$("#userCost").change(function () {
    calculteTotal();
});

$("#gain").change(function () {
    calculteTotal();
});

$("#payment").change(function () {
    calculteTotal();
});

function calculteTotal() {
    let userCost = 0;
    if (!textBoxNumericIsEmpty("userCost")) {
        userCost = parseFloat($("#userCost").val());
    }
    let gain = 0;
    if (!textBoxNumericIsEmpty("gain")) {
        gain = parseFloat($("#gain").val());
    }
    let totalCost = userCost + gain;
    $("#totalCost").val(totalCost);
    let payment = 0;
    if (!textBoxNumericIsEmptyExceptZero("payment")) {
        payment = parseFloat($("#payment").val());
    }
    let balance = totalCost - payment;
    $("#balance").val(balance);
}

function setUserSelect() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/profile/read";
    $.post({
        url: url,
        headers: headers,
        statusCode: {
            200: (response) => {
                let selectHtml = getSelectHtml(response.profiles);
                $("#userId").html(selectHtml);
            },
            400: (response) => {
                console.log(response);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function getSelectHtml(profiles) {
    let html = `<option value=""hidden></option>`;
    let first = true;
    profiles.forEach((profile) => {
        if (first) {
            html += `<option value="${profile.userId}" selected>${profile.lastName} ${profile.name} (${profile.userId})</option>`;
            first = false;
        } else
            html += `<option value="${profile.userId}">${profile.lastName} ${profile.name} (${profile.userId})</option>`;
    });
    return html;
}

function setReceptionDate() {
    let date = new Date();
    fillDateInput("receptionDate", date);
}

const pending = document.getElementById("pendingTasks");
const doing = document.getElementById("doingTasks");
const done = document.getElementById("doneTasks");

Sortable.create(pending, {
    group: "shared",
    animation: 200,
    // quitar fantasma
    // clase para cuando agamos click
    chosenClass: "seleccion",
    // ghostClass: 'fantasma',
    dragClass: "drago",
    // Metodos de Sortable
    onEnd: (response) => {
        // funcion que se ejecuta cuando el usuario suelte lo que este arrastrando
        changingControl(response);
    },
});

Sortable.create(doing, {
    group: "shared",
    animation: 200,
    // quitar fantasma
    // clase para cuando agamos click
    chosenClass: "seleccion",
    // ghostClass: 'fantasma',
    dragClass: "drago",
    // Metodos de Sortable
    onEnd: (response) => {
        // funcion que se ejecuta cuando el usuario suelte lo que este arrastrando
        changingControl(response);
    },
});

Sortable.create(done, {
    group: {
        name: "shared",
        pull: false,
        //put: false, // Do not allow items to be put into this list
    },
    animation: 200,
    sort: false,
    // quitar fantasma
    // clase para cuando agamos click
    chosenClass: "seleccion",
    // ghostClass: 'fantasma',
    dragClass: "drago",
    // Metodos de Sortable
    onEnd: (response) => {
        // funcion que se ejecuta cuando el usuario suelte lo que este arrastrando
        changingControl(response);
    },
});

function changingControl(response) {
    let from = response.from.id;
    let to = response.to.id;
    let route = from + "-" + to;
    let taskId = response.item.id.split("-")[1];
    let task = $("#" + response.item.id);
    actualTask = task;
    prevBoard = from;
    actualTaskId = taskId;
    switch (route) {
        case "pendingTasks-doingTasks":
            $("#newPayment").modal("show");
            $("#newPaymentModalCenterTitle").html(`Nuevo Pago (${taskId})`);
            break;
        case "pendingTasks-doneTasks":
            $("#scoreModal").modal("show");
            $("#scoreModalCenterTitle").html(`Finalizar Tarea (${taskId})`);
            break;
        case "doingTasks-doneTasks":
            $("#scoreModal").modal("show");
            $("#scoreModalCenterTitle").html(`Finalizar Tarea (${taskId})`);
            break;
        case "doingTasks-pendingTasks":
            $("#deletePaymentsModal").modal("show");
            $("#deletePaymentsModalCenterTitle").html(
                `Borrar Pagos (${taskId})`
            );
            break;
    }
}

function read() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/task/read";
    $.post({
        url: url,
        headers: headers,
        success: (response) => {
            fillTable(response.tasks);
            $("#loading").fadeOut(500);
        },
    });
}

function fillTable(tasks) {
    $("#pendingTasks").html("");
    $("#doingTasks").html("");
    $("#doneTasks").html("");
    let doingQuantity = 0;
    let doneQuantity = 0;
    tasks.forEach((task) => {
        let color = getColor(task.type);
        let receptionDate = dateWithNoSeconds(task.receptionDate);
        let deliveryDate = dateWithNoSeconds(task.deliveryDate);
        let html = `<div class="card" id="task-${task.taskId}">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-9">
                                    <a href="#" onclick="showTaskModal(event,${task.taskId})">
                                        <h4 class="text-c-${color} h5 font-weight-bold">
                                            ${task.title} (${task.taskId})
                                        </h4>
                                    </a>
                                    <h6 class="text-muted m-b-0 h6">
                                        ${task.description}
                                    </h6>
                                </div>
                                <div class="col-3 text-right">
                                    <img
                                        src="assets/images/users/${task.photo}"
                                        alt=""
                                        class="img-fluid rounded-circle"
                                        width="50"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="${task.name} ${task.lastName}"
                                    />
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-c-${color}">
                            <div class="row align-items-center">
                                <div class="col-4">
                                    <p class="text-white m-b-0 text-left">
                                        ${receptionDate}
                                    </p>
                                </div>
                                <div class="col-4">
                                    <p class="text-white m-b-0 text-center">
                                        ${deliveryDate}
                                    </p>
                                </div>
                                <div class="col-4 text-right">
                                    <span class="h5 text-white">${task.finalCost} Bs.</span>
                                </div>
                            </div>
                        </div>
                    </div>`;
        switch (task.status) {
            case "0":
                $("#pendingTasks").append(html);
                break;
            case "1":
                $("#doneTasks").append(html);
                doneQuantity += 1;
                break;
            case "2":
                $("#doingTasks").append(html);
                doingQuantity += 1;
                break;
        }
    });

    $("#doingQuantity").html(doingQuantity);
    $("#doneQuantity").html(doneQuantity);
}

function showTaskModal(e, taskId) {
    e.preventDefault();
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/task/findById";
    let data = { taskId: taskId };
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                let task = response.task;
                $("#profilePhoto").attr(
                    "src",
                    "assets/images/users/" + task.photo
                );
                $("#profileUserId").html(taskId);
                $("#profileName").html(task.name);
                $("#profileLastName").html(task.lastName);
                $("#profileTitle").html(task.title);
                $("#profileUserCost").html(task.userCost);
                $("#profileGain").html(task.gain);
                $("#profileFinalCost").html(task.finalCost);
                let receptionDate = dateWithNoSeconds(task.receptionDate);
                $("#profileReceptionDate").html(receptionDate);
                let deliveryDate = dateWithNoSeconds(task.deliveryDate);
                $("#profileDeliveryDate").html(deliveryDate);
                let type = getTaskTypeHtml(task.type);
                $("#profileType").html(type);
                $("#profileSubject").html(task.subject);
                $("#profileClient").html(task.client);
                $("#profilePhone").html(
                    `<a href="https://wa.me/591${task.phone}" target="_blank">${task.phone}</a>`
                );
                let status = getTaskStatusHtml(task.status);
                $("#profileStatus").html(status);
                $("#profileDescription").html(task.description);
                $("#profileTaskId").html(task.taskId);
                $("#profileScore").html(task.score);
                $("#profileBalance").html(task.balance);

                fillTaskPayments(response.payments);

                $("#taskModal").modal("show");
            },
            400: (response) => {
                console.log(response);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function getColor(type) {
    switch (type) {
        case "1":
            return "blue";
        case "2":
            return "red";
    }
}

$("#addTask").on("click", function () {
    setUserSelect();
    setClientSelect();
    $("#newTaskModal").modal("show");
});

$("#newPayment").on("hidden.bs.modal", function () {
    if (!acceptBtn) $("#" + prevBoard).append(actualTask);
    acceptBtn = false;
});

$("#cancelPaymentBtn").on("click", function () {
    $("#newPayment").modal("hide");
    $("#" + prevBoard).append(actualTask);
});

$("#cancelDeletePaymentsBtn").on("click", function () {
    $("#deletePaymentsModal").modal("hide");
    $("#" + prevBoard).append(actualTask);
});

$("#deletePaymentsModal").on("hidden.bs.modal", function () {
    if (!acceptBtn) $("#" + prevBoard).append(actualTask);
    acceptBtn = false;
});

$("#cancelScoreBtn").on("click", function () {
    $("#scoreModal").modal("hide");
    $("#" + prevBoard).append(actualTask);
});

$("#scoreModal").on("hidden.bs.modal", function () {
    if (!acceptBtn) $("#" + prevBoard).append(actualTask);
    acceptBtn = false;
});

$("#deletePaymentsBtn").on("click", function (e) {
    e.preventDefault();
    deletePayments();
});

function deletePayments() {
    let taskId = actualTaskId;
    let url = "api/payment/deletePaymentsExceptFirst";
    let data = { taskId: taskId };
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    $.post({
        url: url,
        data: data,
        headers: headers,
        statusCode: {
            200: (response) => {
                acceptBtn = true;
                $("#deletePaymentsModal").modal("hide");
                Swal.fire({
                    type: "success",
                    title: "CORRECTO",
                    text: "¡Pagos eliminados exitosamente!",
                    animation: true,
                }).then((response) => {});
            },
            400: (response) => {
                console.log(response);
            },
        },
    });
}

$("#createPaymentBtn").on("click", function (e) {
    e.preventDefault();
    if (errorOnPayment()) return;
    createPayment();
});

function errorOnPayment() {
    let anyError = false;
    let errors = Array();
    errors.push(textBoxNumericIsEmpty("amount"));
    errors.forEach((error) => {
        if (error) anyError = true;
    });
    return anyError;
}

function createPayment() {
    let taskId = actualTaskId;
    let amount = $("#amount").val();
    let url = "api/payment/create";
    let data = { taskId: taskId, amount: amount };
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    $.post({
        url: url,
        data: data,
        headers: headers,
        statusCode: {
            200: (response) => {
                acceptBtn = true;
                $("#newPayment").modal("hide");
                Swal.fire({
                    type: "success",
                    title: "CORRECTO",
                    text: "¡Saldo insertado exitosamente!",
                    animation: true,
                }).then((response) => {});
                clean();
                $("#amount").val("");
            },
            400: (response) => {
                console.log(response);
            },
        },
    });
}

$("#createBtn").on("click", function (e) {
    e.preventDefault();
    if (error()) return;
    create();
});

function error() {
    let anyError = false;
    let errors = Array();
    errors.push(textBoxNumericIsEmptyExceptZero("payment"));
    errors.push(textBoxNumericIsEmpty("gain"));
    errors.push(textBoxNumericIsEmpty("userCost"));
    errors.push(textBoxIsEmpty("deliveryDateTime"));
    errors.push(textBoxIsEmpty("deliveryDate"));
    errors.push(textBoxIsEmpty("receptionDateTime"));
    errors.push(textBoxIsEmpty("receptionDate"));
    errors.push(textBoxIsEmpty("subject"));
    errors.push(textBoxIsEmpty("title"));
    if (newClient) errors.push(textBoxIsEmpty("client"));
    errors.forEach((error) => {
        if (error) anyError = true;
    });
    return anyError;
}

function create() {
    let headers = getHeaders();
    let data = getJsonForm("form", [
        "userCost",
        "gain",
        "payment",
        "receptionDate",
        "receptionDateTime",
        "deliveryDate",
        "deliveryDateTime",
        "type",
        "userId",
        "clientId",
        "phone",
    ]);
    data.newClient = newClient;
    let url = "api/task/create";
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                read();
                $("#newTaskModal").modal("hide");
                Swal.fire({
                    type: "success",
                    title: "CORRECTO",
                    text: "Tarea añadida exitosamente!",
                    animation: true,
                });
                setClientSelect();
                clean();
            },
            400: (response) => {
                console.log(response);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

$("#createScoreBtn").on("click", function (e) {
    e.preventDefault();
    updateScore();
});

function updateScore() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let score = $("input:radio[name=stars]:checked").val();
    let taskId = actualTaskId;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let data = {
        taskId: taskId,
        score: score,
    };
    let url = "api/task/finish";
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                acceptBtn = true;
                $("#scoreModal").modal("hide");
                Swal.fire({
                    type: "success",
                    title: "CORRECTO",
                    text: "Tarea finalizada exitosamente!",
                    animation: true,
                });
            },
            400: (response) => {
                console.log(response);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}
