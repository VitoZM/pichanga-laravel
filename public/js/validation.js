function textBoxIsEmpty(id) {
    let input = $("#" + id)
        .val()
        .trim();
    if (input == "") {
        $(`#${id}ErrorMessage`).removeClass("d-none");
        $(`#${id}ErrorMessage`).addClass("d-block");
        $("#" + id).focus();
        return true;
    } else {
        $(`#${id}ErrorMessage`).removeClass("d-block");
        $(`#${id}ErrorMessage`).addClass("d-none");
        return false;
    }
}

function textBoxNumericIsEmpty(id) {
    let input = $("#" + id).val();
    let number = isNaN(parseFloat(input)) ? 0 : parseFloat(input);
    if (number <= 0) {
        $(`#${id}ErrorMessage`).removeClass("d-none");
        $(`#${id}ErrorMessage`).addClass("d-block");
        $("#" + id).focus();
        return true;
    } else {
        $(`#${id}ErrorMessage`).removeClass("d-block");
        $(`#${id}ErrorMessage`).addClass("d-none");
        return false;
    }
}

function textBoxNumericIsEmptyExceptZero(id) {
    let input = $("#" + id).val();
    let number = isNaN(parseFloat(input)) ? 0 : parseFloat(input);
    if (number < 0) {
        $(`#${id}ErrorMessage`).removeClass("d-none");
        $(`#${id}ErrorMessage`).addClass("d-block");
        $("#" + id).focus();
        return true;
    } else {
        $(`#${id}ErrorMessage`).removeClass("d-block");
        $(`#${id}ErrorMessage`).addClass("d-none");
        return false;
    }
}

function uniqueAjaxValidation(url, id) {
    let exists = false;
    let input = $(`#${id}`).val().trim();
    let data = {};
    data[id] = input;
    $.post({
        url: url,
        data: data,
        async: false,
        statusCode: {
            200: (response) => {
                console.log(response);
                exists = true;
                $(`#${id}UniqueErrorMessage`).removeClass("d-none");
                $(`#${id}UniqueErrorMessage`).addClass("d-block");
                $(`#${id}UniqueErrorMessage`).focus();
            },
            201: (response) => {
                console.log(response);
                $(`#${id}UniqueErrorMessage`).removeClass("d-block");
                $(`#${id}UniqueErrorMessage`).addClass("d-none");
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
    return exists;
}

function uniqueOnUpdateAjaxValidation(url, id, userId) {
    let exists = false;
    let input = $(`#${id}`).val().trim();
    let data = {
        userId: userId,
    };
    data[id] = input;
    $.post({
        url: url,
        data: data,
        async: false,
        statusCode: {
            200: (response) => {
                exists = true;
                $(`#${id}UniqueErrorMessage`).removeClass("d-none");
                $(`#${id}UniqueErrorMessage`).addClass("d-block");
                $(`#${id}UniqueErrorMessage`).focus();
            },
            201: (response) => {
                console.log(response);
                $(`#${id}UniqueErrorMessage`).removeClass("d-block");
                $(`#${id}UniqueErrorMessage`).addClass("d-none");
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
    console.log(exists);
    return exists;
}

function withoutKnowledges() {
    noKnowledges = true;
    for (let i = 1; i <= knowledgeId; i++) {
        let knowledge = $("#knowledge" + i)
            .val()
            .trim();
        if (knowledge == "") {
            $(`#knowledge${i}ErrorMessage`).removeClass("d-none");
            $(`#knowledge${i}ErrorMessage`).addClass("d-block");
            removeKnowledgeWithoutBtn(i);
            $("#knowledge" + i).focus();
        } else {
            if (knowledge != "!-/") {
                $(`#knowledge${i}ErrorMessage`).addClass("d-none");
                $(`#knowledge${i}ErrorMessage`).removeClass("d-block");
                noKnowledges = false;
            }
        }
    }
    return noKnowledges;
}
//exceptions es un arreglo que indica que campos no formalizar a json porque son especiales
function getJsonForm(formName, exceptions) {
    var form = $("#" + formName).serializeArray();
    var jsonForm = {};
    $.each(form, function (i, v) {
        if (exceptions.find((exception) => exception == v.name))
            jsonForm[v.name] = v.value.trim();
        else jsonForm[v.name] = format(v.value);
    });
    return jsonForm;
}

function clean() {
    $("#form")[0].reset();
    $(".error-message").removeClass("d-block");
    $(".error-message").addClass("d-none");
}

function format(string) {
    let result = string.trim().toLowerCase();
    result = result.replace(/\b\w/g, (l) => l.toUpperCase());
    return result;
}

function getKnowledges() {
    let knowledges = Array();
    for (let i = 1; i <= knowledgeId; i++)
        if ($("#knowledge" + i).val() != "!-/")
            knowledges.push(format($("#knowledge" + i).val()));
    return knowledges;
}

$(document).on("wheel", "input[type=number]", function (e) {
    $(this).blur();
});
