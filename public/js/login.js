isLogged();

$("#loginBtn").on("click", (event) => {
    event.preventDefault();
    if (error()) return;
    $("#loader").removeClass("d-none");
    $("#loader").fadeIn();
    login();
});

function isLogged() {
    if (sessionStorage.token || localStorage.token)
        window.location = "dashboard";
}

function error() {
    let passwordError = textBoxIsEmpty("password");
    let userNameError = textBoxIsEmpty("userName");

    if (userNameError || passwordError) return true;
    return false;
}

function validateUserName() {
    let userName = $("#userName").val().trim();
    if (userName == "") {
        $("#userNameErrorMessage").removeClass("d-none");
        $("#userNameErrorMessage").addClass("d-block");
        $("#userName").focus();
        return true;
    } else {
        $("#userNameErrorMessage").removeClass("d-block");
        $("#userNameErrorMessage").addClass("d-none");
        return false;
    }
}

function validatePassword() {
    let password = $("#password").val().trim();
    if (password == "") {
        $("#passwordErrorMessage").removeClass("d-none");
        $("#passwordErrorMessage").addClass("d-block");
        $("#password").focus();
        return true;
    } else {
        $("#passwordErrorMessage").removeClass("d-block");
        $("#passwordErrorMessage").addClass("d-none");
        return false;
    }
}

function login() {
    let url = "api/auth/login";
    let userName = $("#userName").val().trim();
    let password = $("#password").val();
    let data = {
        userName: userName,
        password: password,
    };
    $.post({
        url: url,
        data: data,
        statusCode: {
            401: (response) => {
                $("#loader").fadeOut();
                $("#authenticateErrorMessage").removeClass("d-none");
                $("#authenticateErrorMessage").removeClass("d-block");
            },
            200: (response) => {
                localStorage.profile = JSON.stringify(response.profile);
                createLog(response.token);
            },
        },
    });
}

function createSession(token) {
    if ($("#rememberMe").prop("checked")) localStorage.token = token;
    else sessionStorage.token = token;
}

function createLog(token) {
    let url = "api/log/create";
    let headers = {
        Authorization: "Bearer " + token,
    };
    let data = {
        token: token,
    };
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                createSession(token);
                window.location = "dashboard";
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}
