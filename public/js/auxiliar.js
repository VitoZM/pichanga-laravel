isLogged("create-users");
var knowledgesOnView = 1;
var knowledgeId = 1;

$("#createBtn").click(function (e) {
    e.preventDefault();
    if (error()) return;
    create();
});

function error() {
    let anyError = false;
    let errors = Array();
    errors.push(withoutKnowledges());
    errors.push(textBoxIsEmpty("bankAccount"));
    errors.push(textBoxIsEmpty("email"));
    errors.push(uniqueAjaxValidation("api/profile/alreadyUsedEmail", "email"));
    errors.push(textBoxIsEmpty("phone"));
    errors.push(textBoxIsEmpty("city"));
    errors.push(textBoxIsEmpty("ci"));
    errors.push(uniqueAjaxValidation("api/profile/alreadyUsedCi", "ci"));
    errors.push(textBoxIsEmpty("lastName"));
    errors.push(textBoxIsEmpty("name"));
    errors.forEach((error) => {
        if (error) anyError = true;
    });
    return anyError;
}

function create() {
    let token = sessionStorage.token
        ? sessionStorage.token
        : localStorage.token;
    let url = "api/user/create";
    let data = getJsonForm("form", [
        "ci",
        "issued",
        "phone",
        "bankAccount",
        "bank",
    ]);
    let headers = {
        Authorization: "Bearer " + token,
    };
    data.email = data.email.toLowerCase();
    data.knowledges = JSON.stringify(getKnowledges());
    $.post({
        url: url,
        data: data,
        headers: headers,
        statusCode: {
            200: (response) => {
                Swal.fire({
                    type: "success",
                    title: "CORRECTO",
                    text: "¡Auxiliar añadido exitosamente!",
                    animation: true,
                }).then(() => {
                    clean();
                });
            },
            500: (response) => {},
        },
    });
}

function addKnowledge(e) {
    e.preventDefault();
    knowledgesOnView++;
    knowledgeId++;
    let html = `<div data-role="appendRow" id="knowledgeDiv${knowledgeId}">
                    <div class="form-inline form-group">
                        <input input type="text"
                            class="form-control mb-2 mr-sm-2 col-12 col-sm-9 col-md-8 col-lg-9"
                            id="knowledge${knowledgeId}" placeholder="Otra area de conocimiento">

                        <!-- file upload ends-->
                        <div class="mx-auto">
                            <button
                                class="btn btn-sm btn-outline-danger rounded-circle mr-2 mb-2 botoncitoX"
                                onclick="removeKnowledge(event,${knowledgeId})">
                                <i class="feather icon-x"></i>
                            </button>
                            <button
                                class="btn btn-sm btn-outline-primary rounded-circle mb-2 botoncitoMas"
                                onclick="addKnowledge(event);">
                                <i class="feather icon-plus"></i>
                            </button>
                        </div>
                        <p class="text-danger small m-0 d-none"
                            id="knowledge${knowledgeId}ErrorMessage"><i
                                class="feather icon-alert-circle"></i> Debe introducir al menos un conocimiento
                        </p>
                    </div> <!-- /div.form-inline -->
                </div>`;
    $("#knowledges").append(html);
}

function removeKnowledge(e, knowledgeId) {
    e.preventDefault();
    if (knowledgesOnView == 1) return;
    $("#knowledgeDiv" + knowledgeId).fadeOut();
    $("#knowledge" + knowledgeId).val("!-/");
    knowledgesOnView--;
}

function removeKnowledgeWithoutBtn(knowledgeId) {
    if (knowledgesOnView == 1) return;
    $("#knowledgeDiv" + knowledgeId).fadeOut();
    $("#knowledge" + knowledgeId).val("!-/");
    knowledgesOnView--;
}
