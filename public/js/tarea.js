isLogged("create-tasks");
setReceptionDate();
$(document).ready(function () {
    setClientSelect();
    setUserSelect();
});
var clientsArray = [];
var newClient = false;

$("#userCost").change(function () {
    calculteTotal();
});

$("#gain").change(function () {
    calculteTotal();
});

$("#payment").change(function () {
    calculteTotal();
});

function calculteTotal() {
    let userCost = 0;
    if (!textBoxNumericIsEmpty("userCost")) {
        userCost = parseFloat($("#userCost").val());
    }
    let gain = 0;
    if (!textBoxNumericIsEmpty("gain")) {
        gain = parseFloat($("#gain").val());
    }
    let totalCost = userCost + gain;
    $("#totalCost").val(totalCost);
    let payment = 0;
    if (!textBoxNumericIsEmptyExceptZero("payment")) {
        payment = parseFloat($("#payment").val());
    }
    let balance = totalCost - payment;
    $("#balance").val(balance);
}

function setUserSelect() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/profile/read";
    $.post({
        url: url,
        headers: headers,
        statusCode: {
            200: (response) => {
                let selectHtml = getSelectHtml(response.profiles);
                $("#userId").html(selectHtml);
            },
            400: (response) => {
                console.log(response);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function getSelectHtml(profiles) {
    let html = `<option value=""hidden></option>`;
    let first = true;
    profiles.forEach((profile) => {
        if (first) {
            html += `<option value="${profile.userId}" selected>${profile.lastName} ${profile.name} (${profile.userId})</option>`;
            first = false;
        } else
            html += `<option value="${profile.userId}">${profile.lastName} ${profile.name} (${profile.userId})</option>`;
    });
    return html;
}

function setReceptionDate() {
    let date = new Date();
    fillDateInput("receptionDate", date);
}

$("#createBtn").on("click", function (e) {
    e.preventDefault();
    if (error()) return;
    create();
});

function error() {
    let anyError = false;
    let errors = Array();
    errors.push(textBoxNumericIsEmptyExceptZero("payment"));
    errors.push(textBoxNumericIsEmpty("gain"));
    errors.push(textBoxNumericIsEmpty("userCost"));
    errors.push(textBoxIsEmpty("deliveryDateTime"));
    errors.push(textBoxIsEmpty("deliveryDate"));
    errors.push(textBoxIsEmpty("receptionDateTime"));
    errors.push(textBoxIsEmpty("receptionDate"));
    errors.push(textBoxIsEmpty("subject"));
    errors.push(textBoxIsEmpty("title"));
    if (newClient) errors.push(textBoxIsEmpty("client"));
    errors.forEach((error) => {
        if (error) anyError = true;
    });
    return anyError;
}

function create() {
    let headers = getHeaders();
    let data = getJsonForm("form", [
        "userCost",
        "gain",
        "payment",
        "receptionDate",
        "receptionDateTime",
        "deliveryDate",
        "deliveryDateTime",
        "type",
        "userId",
        "clientId",
        "phone",
    ]);
    data.newClient = newClient;
    let url = "api/task/create";
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                Swal.fire({
                    type: "success",
                    title: "CORRECTO",
                    text: "Tarea añadida exitosamente!",
                    animation: true,
                });
                setClientSelect();
                clean();
            },
            400: (response) => {
                console.log(response);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}
