function getDay(date) {
    let day = date.getDate();
    return parseInt(day) < 10 ? "0" + day : day;
}

function getMonth(date) {
    let month = parseInt(date.getMonth()) + 1;
    return month < 10 ? "0" + month : month;
}

function getHours(date) {
    let hours = date.getHours();
    return parseInt(hours) < 10 ? "0" + hours : hours;
}

function getMinutes(date) {
    let minutes = date.getMinutes();
    return parseInt(minutes) < 10 ? "0" + minutes : minutes;
}

function fillDateInput(id, date) {
    let day = getDay(date);
    let month = getMonth(date);
    let year = date.getFullYear();
    let inputDate = `${year}-${month}-${day}`;
    let hours = getHours(date);
    let minutes = getMinutes(date);
    let inputDateTime = `${hours}:${minutes}`;
    $("#" + id).val(inputDate);
    $("#" + id + "Time").val(inputDateTime);
}

function dateWithNoSeconds(date) {
    date = new Date(date);
    let hours = getHours(date);
    let minutes = getMinutes(date);
    date = getDMYdate(date);
    return `${date} ${hours}:${minutes}`;
}

function getDMYdate(date) {
    date = new Date(date);
    let day = getDay(date);
    let month = getMonth(date);
    let year = date.getFullYear();

    return `${day}/${month}/${year}`;
}

function getHeaders() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    return { Authorization: "Bearer " + token };
}
