isLogged("create-messages");

$("#createBtn").click(function (e) {
    e.preventDefault();
    if (error()) return;
    create();
});

function error() {
    let anyError = false;
    let errors = Array();
    errors.push(textBoxIsEmpty("title"));
    errors.push(textBoxIsEmpty("description"));
    errors.forEach((error) => {
        if (error) anyError = true;
    });
    return anyError;
}

function create() {
    let token = sessionStorage.token
        ? sessionStorage.token
        : localStorage.token;
    let url = "api/message/create";
    let title = $("#title").val();
    let description = $("#description").val();
    let data = {
        title: title,
        description,
    };
    let headers = {
        Authorization: "Bearer " + token,
    };
    $.post({
        url: url,
        data: data,
        headers: headers,
        statusCode: {
            200: (response) => {
                Swal.fire({
                    type: "success",
                    title: "CORRECTO",
                    text: "¡Mensaje añadido exitosamente!",
                    animation: true,
                }).then(() => {
                    clean();
                });
                fillMessageButtonsByAjax();
            },
            500: (response) => {},
        },
    });
}
