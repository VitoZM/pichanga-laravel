isLogged("dashboard");
read();
function read() {
    let headers = getHeaders();
    let url = "api/log/dashboard";
    $.post({
        url: url,
        headers: headers,
        statusCode: {
            200: (response) => {
                let userCount = `<i class="feather icon-users mr-2 text-primary"></i>${response.userCount}`;
                let taskCount = `<i class="feather icon-file-text mr-2 text-success"></i>${response.taskCount}`;
                let clientCount = `<i class="feather icon-user-plus mr-2 text-warning"></i>${response.clientCount}`;
                let pendingTasks = response.pendingTasks;
                let doingTasks = response.doingTasks;
                let doneTasks = response.doneTasks;
                let workTasks = response.workTasks;
                let examTasks = response.examTasks;
                let workScore = `<i class="feather icon-star-on text-warning mr-2 f-16"></i>${response.workScore}`;
                let examScore = `<i class="feather icon-star-on text-warning mr-2 f-16"></i>${response.examScore}`;
                $("#userCount").html(userCount);
                $("#taskCount").html(taskCount);
                $("#clientCount").html(clientCount);
                $("#pendingTasks").html(pendingTasks);
                $("#doingTasks").html(doingTasks);
                $("#doneTasks").html(doneTasks);
                $("#workTasks").html(workTasks);
                $("#examTasks").html(examTasks);
                $("#workScore").html(workScore);
                $("#examScore").html(examScore);
            },
            400: (response) => {
                console.log(response);
            },
            500: (response) => {
                console.log(response);
            },
        },
        success: (response) => {},
    });
}
