var actualTaskId;

isLogged("read-tasks");
read();
setUserSelect();

function read() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/task/read";
    $.post({
        url: url,
        headers: headers,
        statusCode: {
            200: (response) => {
                fillTasks(response.tasks);
                $("#loading").fadeOut(500);
            },
        },
    });
}

function setUserSelect() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/profile/read";
    $.post({
        url: url,
        headers: headers,
        statusCode: {
            200: (response) => {
                let selectHtml = getSelectHtml(response.profiles);
                $("#userId").html(selectHtml);
            },
            400: (response) => {
                console.log(response);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function getSelectHtml(profiles) {
    let html = `<option value=""hidden></option>`;
    let first = true;
    profiles.forEach((profile) => {
        if (first) {
            html += `<option value="${profile.userId}" selected>${profile.lastName} ${profile.name} (${profile.userId})</option>`;
            first = false;
        } else
            html += `<option value="${profile.userId}">${profile.lastName} ${profile.name} (${profile.userId})</option>`;
    });
    return html;
}

function fillTasks(tasks) {
    $("#table").html("");
    tasks.forEach((task) => {
        let taskId = task.taskId;
        let title = task.title;
        let client = task.client;
        let deliveryDate = dateWithNoSeconds(task.deliveryDate);
        let taskType = getTaskTypeHtml(task.type);
        let taskStatus = getTaskStatusHtml(task.status);
        let taskScore = getTaskScoreHtml(task.score);
        let userName = `${task.name} ${task.lastName}`;
        let city = task.city;
        let photo = task.photo;
        let finalCost = task.finalCost;
        let iconsHtml = getIconsHtml(task.status, task.taskId);
        let html = `<tr>
                        <td>${taskId}</td>
                        <td>${title}</td>
                        <td>${client}</td>
                        <td>${deliveryDate}</td>
                        <td>${taskType}</td>
                        <td>
                            <div class="d-inline-block align-middle">
                                <img src="assets/images/users/${photo}"
                                    alt="user image"
                                    class="img-radius wid-40 align-top m-r-15"
                                />
                            </div>
                        </td>
                        <td>
                            <div class="d-inline-block align-middle">
                                <div class="d-inline-block">
                                    <h6>${userName}</h6>
                                    <p class="text-muted m-b-0">${city}</p>
                                </div>
                            </div>
                        </td>
                        <td>${finalCost}</td>
                        <td>${taskStatus}</td>
                        <td>${taskScore}</td>
                        <td>${iconsHtml}</td>
                    </tr>`;

        $("#table").append(html);
    });
}

function getIconsHtml(status, taskId) {
    let html = "";
    if (status == "0" || status == "2")
        html += `<a class="success p-0"
                    type="button"
                    title="Editar Tarea"
                    onclick="showEditModal(${taskId})"
                >
                    <i class="feather icon-edit"></i>
                </a>
                <a class="success p-0"
                    type="button"
                    title="Eliminar Tarea"
                    onclick="showDeleteModal(${taskId})"
                >
                    <i class="feather icon-x"></i>
                </a>
                <a class="success p-0"
                    type="button"
                    title="Añadir Pago"
                    onclick="showTaskBalanceModal(${taskId})"
                >
                    <i class="feather icon-credit-card"></i>
                </a>`;
    html += ` <a class="success p-0"
                type="button"
                title="Ver Tarea"
                onclick="showTaskModal(${taskId})"
            >
                <i class="feather icon-eye"></i>
            </a>`;
    return html;
}

function showTaskModal(taskId) {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/task/findById";
    let data = { taskId: taskId };
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                let task = response.task;
                $("#profilePhoto").attr(
                    "src",
                    "assets/images/users/" + task.photo
                );
                $("#profileUserId").html(taskId);
                $("#profileName").html(task.name);
                $("#profileLastName").html(task.lastName);
                $("#profileTitle").html(task.title);
                $("#profileUserCost").html(task.userCost);
                $("#profileGain").html(task.gain);
                $("#profileFinalCost").html(task.finalCost);
                let receptionDate = dateWithNoSeconds(task.receptionDate);
                $("#profileReceptionDate").html(receptionDate);
                let deliveryDate = dateWithNoSeconds(task.deliveryDate);
                $("#profileDeliveryDate").html(deliveryDate);
                let type = getTaskTypeHtml(task.type);
                $("#profileType").html(type);
                $("#profileSubject").html(task.subject);
                $("#profileClient").html(task.client);
                $("#profilePhone").html(
                    `<a href="https://wa.me/591${task.phone}" target="_blank">${task.phone}</a>`
                );
                let status = getTaskStatusHtml(task.status);
                $("#profileStatus").html(status);
                $("#profileDescription").html(task.description);
                $("#profileTaskId").html(task.taskId);
                $("#profileScore").html(task.score);
                $("#profileBalance").html(task.balance);

                fillTaskPayments(response.payments);

                $("#taskModal").modal("show");
            },
            400: (response) => {
                console.log(response);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function showTaskBalanceModal(taskId) {
    actualTaskId = taskId;
    let token = sessionStorage.token
        ? sessionStorage.token
        : localStorage.token;
    let url = "api/task/getTitleAndPayments";
    let data = { taskId: taskId };
    let headers = {
        Authorization: "Bearer " + token,
    };
    $.post({
        url: url,
        data: data,
        headers: headers,
        statusCode: {
            200: (response) => {
                let task = response.task;
                let payments = response.payments;
                $("#paymentsModalCenterTitle").html(
                    `${task.title} (${taskId})`
                );
                let html = "";
                payments.forEach((payment) => {
                    let date = dateWithNoSeconds(payment.created_at);
                    if (payment.amount > 0)
                        html += '<tr class="p-3 mb-2 bg-success text-white">';
                    else html += '<tr class="p-3 mb-2 bg-danger text-white">';
                    html += `   <td>${date}</td>
                                <td>${payment.amount} Bs.</td>
                            </tr>`;
                });
                $("#payments").html(html);
                let textColor = "";
                let lastPayment =
                    payments[0] == undefined ? 0 : payments[0].amount;
                if (lastPayment > 0) textColor = "text-success";
                else textColor = "text-danger";
                $("#lastPayment")
                    .html(`<label class="elevacion font-weight-bold text-dark">Último Pago</label>
                                 <p class="text-muted font-weight-bold ${textColor}">${lastPayment} Bs.</p>`);
                if (task.balance > 0) textColor = "text-success";
                else textColor = "text-danger";
                $("#balance")
                    .html(`<label class="elevacion font-weight-bold text-dark">Saldo Restante</label>
                            <p class="text-muted font-weight-bold ${textColor}">${task.balance} Bs.</p>`);
                $("#taskBalanceModal").modal("show");
            },
            500: (response) => {},
        },
    });
}

function showDeleteModal(taskId) {
    actualTaskId = taskId;
    $("#deleteModalCenterTitle").html(`Eliminar Tarea (${taskId})`);
    $("#deleteTaskModal").modal("show");
}

function showEditModal(taskId) {
    actualTaskId = taskId;
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/task/findById";
    let data = { taskId: taskId };
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                clean();
                fillEditForm(response.task);
                $("#editModal").modal("show");
            },
            400: (response) => {
                console.log(response);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

function fillEditForm(task) {
    $("#editModalCenterTitle").html(`Editar Tarea (${task.taskId})`);
    //con este for llenamos todo un form al toque mi rey
    for (var key in task) $("#" + key).val(task[key]);
    let receptionDate = new Date(task.receptionDate);
    fillDateInput("receptionDate", receptionDate);
    let deliveryDate = new Date(task.deliveryDate);
    fillDateInput("deliveryDate", deliveryDate);
}

$("#updateBtn").on("click", function (e) {
    e.preventDefault();
    if (error()) return;
    update();
});

$("#deleteBtn").on("click", function (e) {
    e.preventDefault();
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let url = "api/task/delete";
    let data = { taskId: actualTaskId };
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                read();
                $("#deleteTaskModal").modal("hide");
                Swal.fire({
                    type: "success",
                    title: "CORRECTO",
                    text: "¡Tarea eliminada exitosamente!",
                    animation: true,
                });
            },
            400: (response) => {
                console.log(response);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
});

function error() {
    let anyError = false;
    let errors = Array();
    errors.push(textBoxIsEmpty("client"));
    errors.push(textBoxIsEmpty("subject"));
    errors.push(textBoxIsEmpty("deliveryDateTime"));
    errors.push(textBoxIsEmpty("deliveryDate"));
    errors.push(textBoxIsEmpty("receptionDateTime"));
    errors.push(textBoxIsEmpty("receptionDate"));
    errors.push(textBoxNumericIsEmptyExceptZero("payment"));
    errors.push(textBoxNumericIsEmpty("gain"));
    errors.push(textBoxNumericIsEmpty("userCost"));
    errors.push(textBoxIsEmpty("title"));
    errors.forEach((error) => {
        if (error) anyError = true;
    });
    return anyError;
}

function update() {
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    let taskId = actualTaskId;
    let data = getJsonForm("form", [
        "userCost",
        "gain",
        "payment",
        "receptionDate",
        "receptionDateTime",
        "deliveryDate",
        "deliveryDateTime",
        "type",
        "userId",
        "phone",
    ]);
    data.taskId = taskId;
    let url = "api/task/update";
    $.post({
        url: url,
        headers: headers,
        data: data,
        statusCode: {
            200: (response) => {
                $("#closeEditModal").click();
                read();
                Swal.fire({
                    type: "success",
                    title: "CORRECTO",
                    text: "¡Auxiliar añadido exitosamente!",
                    animation: true,
                });
            },
            400: (response) => {
                console.log(response);
            },
            500: (response) => {
                console.log(response);
            },
        },
    });
}

$("#userCost").change(function () {
    calculteTotal();
});

$("#gain").change(function () {
    calculteTotal();
});

function calculteTotal() {
    let userCost = 0;
    if (!textBoxNumericIsEmpty("userCost")) {
        userCost = parseFloat($("#userCost").val());
    }
    let gain = 0;
    if (!textBoxNumericIsEmpty("gain")) {
        gain = parseFloat($("#gain").val());
    }
    let totalCost = userCost + gain;
    $("#finalCost").val(totalCost);
}

$("#createPaymentBtn").on("click", function (e) {
    e.preventDefault();
    if (errorOnPayment()) return;
    createPayment();
});

function errorOnPayment() {
    let anyError = false;
    let errors = Array();
    errors.push(textBoxNumericIsEmpty("amount"));
    errors.forEach((error) => {
        if (error) anyError = true;
    });
    return anyError;
}

function createPayment() {
    let taskId = actualTaskId;
    let amount = $("#amount").val();
    let url = "api/payment/create";
    let data = { taskId: taskId, amount: amount };
    let token = localStorage.token ? localStorage.token : sessionStorage.token;
    let headers = {
        Authorization: "Bearer " + token,
    };
    $.post({
        url: url,
        data: data,
        headers: headers,
        statusCode: {
            200: (response) => {
                $("#taskBalanceModal").modal("hide");
                $("#newPayment").modal("hide");
                Swal.fire({
                    type: "success",
                    title: "CORRECTO",
                    text: "¡Saldo insertado exitosamente!",
                    animation: true,
                }).then((response) => {
                    showTaskBalanceModal(taskId);
                });
                clean();
                $("#amount").val("");
            },
            400: (response) => {
                console.log(response);
            },
        },
    });
}
