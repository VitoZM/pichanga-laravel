<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    use HasFactory;

    protected $fillable = [
        'balanceId',
        'amount',
        'description',
        'enabled',
        'userId'
    ];

    protected $primaryKey = 'balanceId';
}
