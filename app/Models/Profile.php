<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $fillable = [
        'userId',
        'name',
        'lastName',
        'ci',
        'issued',
        'city',
        'phone',
        'email',
        'photo',
        'bankAccount',
        'bank',
        'knowledges'
    ];

    protected $primaryKey = 'userId';
}
