<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'taskId',
        'title',
        'userCost',
        'gain',
        'finalCost',
        'balance',
        'receptionDate',
        'deliveryDate',
        'type',
        'subject',
        'description',
        'score',
        'status',
        'enabled',
        'userId',
        'clientId'
    ];

    protected $primaryKey = 'taskId';

}
