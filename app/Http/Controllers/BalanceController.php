<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Balance;

class BalanceController extends Controller
{
    //Crear balance para los usuarios desde listar usuarios
    public function create(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('create-balances'))
                return response()->json([
                    'success' => false,
            ], 400);
            if($request->amount <= 0)
                return response()->json(['success' => false], 500);
            $balance = new Balance($request->all());
            $balance->save();
            return response()->json(['success' => true, 'balance' => $balance], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    //crear balance negativo desde taskcontroller por una tarea finalizada
    public static function createBalance($amount,$description,$userId){
        try{
            $user = AuthController::getUser();
            if(!$user->can('create-balances'))
                return response()->json([
                    'success' => false,
            ], 400);
            $balance = new Balance([
                'amount' => -$amount,
                'description' => $description,
                'userId' => $userId
            ]);
            $balance->save();
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }
}
