<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Profile;
use App\Models\Balance;
use App\Models\Task;

class ProfileController extends Controller
{
    public function alreadyUsedCi(Request $request){
        try{
            $ci = $request->ci;
            $profile = Profile::all()->where("ci",$ci)->first();
            if($profile != null)
                return response()->json(['success' => true], 200);
            return response()->json(['success' => false], 201);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function alreadyUsedCiOnUpdate(Request $request){
        try{
            $ci = $request->ci;
            $userId = $request->userId;
            $profile = Profile::all()
                                ->where("ci",$ci)
                                ->where("userId","<>",$userId)
                                ->first();
            if($profile != null)
                return response()->json(['success' => true], 200);
            return response()->json(['success' => false], 201);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function alreadyUsedEmail(Request $request){
        try{
            $email = $request->email;
            $profile = Profile::all()->where("email",$email)->first();
            if($profile != null)
                return response()->json(['success' => true], 200);
            return response()->json(['success' => false], 201);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function alreadyUsedEmailOnUpdate(Request $request){
        try{
            $email = $request->email;
            $userId = $request->userId;
            $profile = Profile::all()
                                ->where("email",$email)
                                ->where("userId","<>",$userId)
                                ->first();
            if($profile != null)
                return response()->json(['success' => true], 200);
            return response()->json(['success' => false], 201);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }/**SELECT u.userId, COALESCE(sum(b.amount),0) as 'balance'
FROM users u LEFT JOIN balances b ON u.userId = b.userId */

    public function findById(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('read-users'))
                return response()->json([
                    'success' => false,
            ], 400);
            $userId = $request->userId;
            $profile = Profile::join('users','users.userId','profiles.userId')
                                ->join('balances','balances.userId','users.userId')
                                ->select(
                                    'profiles.userId','profiles.name','profiles.lastName','profiles.phone',
                                    'profiles.city','profiles.knowledges','profiles.photo','profiles.ci','profiles.issued',
                                    'profiles.email','profiles.bankAccount','profiles.bank',
                                    DB::raw('SUM(balances.amount) as balance')
                                )
                                ->where('users.userId',$userId)
                                ->where('users.enabled','1')
                                ->groupBy(
                                    'profiles.userId','profiles.name','profiles.lastName','profiles.phone','profiles.city',
                                    'profiles.knowledges','profiles.photo','profiles.ci','profiles.issued','profiles.email',
                                    'profiles.bankAccount','profiles.bank'
                                )
                                ->first();
            $profileScore = Profile::join('users','users.userId','profiles.userId')
                                    ->join('tasks','tasks.userId','profiles.userId')
                                    ->select('profiles.userId',DB::raw('SUM(tasks.score)/(COUNT(*)-1) as score'))
                                    ->where('users.userId',$userId)
                                    ->where('users.enabled','1')
                                    ->where('tasks.status','1')
                                    ->groupBy('profiles.userId')
                                    ->first();
            $profile->score = $profileScore->score == null ? 0 : $profileScore->score;
            $tasks = Task::join('clients','clients.clientId','tasks.clientId')
                            ->select('tasks.*','clients.name as client')
                            ->latest()
                            ->where('userId',$userId)
                            ->where('type','<>',9)
                            ->take(3)
                            ->get();
            if($profile != null)
                return response()->json(['success' => true,'profile' => $profile,'tasks' => $tasks], 200);
            return response()->json(['success' => false], 201);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function read(){
        try{
            $user = AuthController::getUser();
            if(!$user->can('read-users'))
                return response()->json([
                    'success' => false,
            ], 400);
            $profiles = Profile::join('users','users.userId','profiles.userId')
                                ->join('balances','balances.userId','users.userId')
                                ->select(
                                    'profiles.userId','profiles.name','profiles.lastName','profiles.phone',
                                    'profiles.city','profiles.knowledges','profiles.photo',
                                    DB::raw('SUM(balances.amount) as balance')
                                )
                                ->where('users.enabled','1')
                                ->groupBy('profiles.userId','profiles.name','profiles.lastName','profiles.phone','profiles.city','profiles.knowledges','profiles.photo')
                                ->get();
            //para sacar el promedio de calificaciones debemos de restar 1 a la cantidad porque una tarea es la apertura de cuenta
            $profilesScore = Profile::join('users','users.userId','profiles.userId')
                                    ->join('tasks','tasks.userId','profiles.userId')
                                    ->select('profiles.userId',DB::raw('SUM(tasks.score)/(COUNT(*)-1) as score'))
                                    ->where('users.enabled','1')
                                    ->where('tasks.status','1')
                                    ->groupBy('profiles.userId')
                                    ->get();
            $j = 0;
            for($i=0;$i<count($profiles);$i++)
                if($profiles[$i]->userId == $profilesScore[$j]->userId){
                    $profiles[$i]->score = $profilesScore[$j]->score == null ? 0 : $profilesScore[$j]->score;
                    $j++;
                }

            return response()->json([
                'response' => 'success',
                'profiles' => $profiles,
            ], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function update(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('update-users'))
                return response()->json([
                    'success' => false,
            ], 400);
            $userId = $request->userId;
            $profile = Profile::all()->where('userId',$userId)->first();
            $profile->fill($request->all());
            $profile->save();
            return response()->json(['response' => 'success'], 200);
        }
        catch (Throwable $t){
            return $t;
        }
    }

    public function getProfileAndBalance(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('read-users'))
                return response()->json([
                    'success' => false,
            ], 400);
            $userId = $request->userId;
            $profile = Profile::join('balances','balances.userId','profiles.userId')
                                ->select(
                                    'profiles.userId','profiles.name','profiles.lastName',
                                    DB::raw('SUM(balances.amount) as balance')
                                )
                                ->where('profiles.userId',$userId)
                                ->groupBy('profiles.userId','profiles.name','profiles.lastName')
                                ->first();
            $balances = Balance::latest()
                                ->where('userId',$userId)
                                ->where('enabled','1')
                                ->latest()//cuidado con esta función, ordena según fecha.
                                ->take(5)
                                ->get();
            return response()->json(['success' => true,'profile' => $profile, 'balances' => $balances], 200);
        }
        catch (Throwable $t){
            return $t;
        }
    }
}
