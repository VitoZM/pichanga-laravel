<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log;
use App\Models\Task;
use App\Models\User;
use App\Models\Client;
use Illuminate\Support\Facades\DB;

class LogController extends Controller
{
    public static function updateJson(){
        $logsJson = Log::join('users','users.userId','=','logs.userId')
                    ->select('users.userName','logs.*')
                    ->orderBy('logs.logId','desc')
                    ->get()
                    ->toJson();
        try{
            $file = fopen("app-assets/data/bitacora-list.json", "w+b");
            fwrite($file, $logsJson);
            // Fuerza a que se escriban los datos pendientes en el buffer:
            fflush($file);
        }catch (Throwable $t){

        }finally{
            fclose($file);
        }
    }

    public function create(){
        try{
            $user = AuthController::getUser();
            if(!$user->can('create-logs'))
                return response()->json([
                    'success' => false,
            ], 400);
            $now = new \DateTime();
            $user = AuthController::getUser();
            $lastLog = Log::all()->where("userId",$user->userId)->last();
            if($lastLog != null){
                if($lastLog->status == "1")
                    self::update();
            }
            $now = $now->format('Y-m-d H:i:s');
            $log = new Log([
                'entry' => $now,
                'departure' => $now,
                'userId' => $user->userId
            ]);
            $log->save();
            return response()->json([
                'success' => true
            ], 200);
        }
        catch (Throwable $t){
            return response()->json([
                'success' => false,
                'error' => $t
            ], 500);
        }
        return $user;
    }

    public function update(){
        try{
            $user = AuthController::getUser();
            if(!$user->can('update-logs'))
                return response()->json([
                    'success' => false,
            ], 400);
            $now = new \DateTime();
            $departure = $now->format('Y-m-d H:i:s');
            $user = AuthController::getUser();
            $log = Log::all()->where("userId",$user->userId)->last();
            $log->departure = $departure;
            $log->status = "0";
            $entry = new \DateTime($log->entry);
            $duration = $entry->diff($now);
            $log->duration = $duration->format('%m meses %d days %h horas %i minutos %s segundos');
            $log->save();
            return response()->json([
                'success' => true
            ], 200);
        }
        catch (Throwable $t){
            return response()->json([
                'success' => false,
                'error' => $t
            ], 500);
        }
        return $user;
    }

    public function read(){
        //self::updateJson();
    }

    public function dashboard(){
        try{
            $user = AuthController::getUser();
            if(!$user->can('read-logs'))
                return response()->json([
                    'success' => false,
            ], 400);
            $users = User::all()->where('enabled','1');
            $usersCount = count($users);
            $tasks = Task::all()->where('type','<>','9');
            $clients = Client::all()->where('enabled','1');
            $clientCount = count($clients);
            $tasksCount = count($tasks);
            $pendingTasks = count($tasks->where('status','0'));
            $doingTasks = count($tasks->where('status','2'));
            $doneTasks = count($tasks->where('status','1'));
            $workTasks = count($tasks->where('type','1'));
            $examTasks = count($tasks->where('type','2'));
            $works = Task::select(DB::raw('SUM(score)/COUNT(*) as score'))
                                ->where('status','1')
                                ->where('type','1')
                                ->first();
            $exams = Task::select(DB::raw('SUM(score)/COUNT(*) as score'))
                                ->where('status','1')
                                ->where('type','2')
                                ->first();
            $workScore = $works->score == null ? 0 : $works->score;
            $examScore = $exams->score == null ? 0 : $exams->score;
            return response()->json([
                'success' => true,
                'userCount' => $usersCount,
                'clientCount' => $clientCount,
                'taskCount' => $tasksCount,
                'pendingTasks' => $pendingTasks,
                'doingTasks' => $doingTasks,
                'doneTasks' => $doneTasks,
                'workTasks' => $workTasks,
                'examTasks' => $examTasks,
                'workScore' => $workScore,
                'examScore' => $examScore
            ], 200);
        }
        catch (Throwable $t){
            return response()->json([
                'success' => false,
                'error' => $t
            ], 500);
        }
    }
}
