<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Profile;
use App\Models\Role;
use App\Models\Balance;
use App\Models\Task;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{

    public static function findByUserName(Request $request){
        $user = AuthController::getUser();
            if(!$user->can('read-users'))
                return response()->json([
                    'success' => false,
            ], 400);
        $userName = strtolower($request->userName);
        $user = User::where('userName',$userName)->first();
        if($user != null)
            return response()->json(['success' => true], 200);
        return response()->json(['success' => false], 500);
    }

    private function createUserProfile($request){
        $profile = new Profile($request->all());
        $insertedUser = User::all()->where('userName',$profile->email)->first();
        $profile->userId = $insertedUser->userId;
        $profile->photo = self::makeAvatar($profile->userId,$profile->name,$profile->lastName);
        $profile->save();
    }

    private static function makeAvatar($userId,$name,$lastName){
        $path = "assets/images/users/$userId.png";
        $image = imagecreate(200, 200);
        $red = rand(0, 255);
        $green = rand(0, 255);
        $blue = rand(0, 255);
        imagecolorallocate($image, $red, $green, $blue);
        $textcolor = imagecolorallocate($image, 255,255,255);

        $name = strtoupper($name)[0];
        $lastName = strtoupper($lastName)[0];
        $character = "$name$lastName";
        //windows
        imagettftext($image, 100, 0, 21, 145, $textcolor, public_path()."\\assets\\fonts\\Cousine-Bold.ttf", $character);
        //hosting
        imagettftext($image, 100, 0, 21, 145, $textcolor, public_path()."/assets/fonts/Cousine-Bold.ttf", $character);
        imagepng($image, $path);
        imagedestroy($image);
        return "$userId.png";
    }

    private function createUserBalance($userId){
        $balance = new Balance([
            'amount' => 0,
            'description' => 'Apertura de cuenta',
            'userId' => $userId
        ]);
        $balance->save();
    }

    private function createUserScore($userId){
        $date = new \DateTime();
        $date = $date->format('Y/m/d');
        $task = new Task([
            'title' => 'Apertura de cuenta',
            'userCost' => 0,
            'gain' => 0,
            'finalCost' => 0,
            'balance' => 0,
            'receptionDate' => $date,
            'deliveryDate' => $date,
            'type' => '9',
            'subject' => 'Ninguno',
            'client' => 'Ninguno',
            'phone' => '-',
            'score' => 0,
            'status' => 1,
            'description' => '',
            'userId' => $userId,
            'clientId' => 1
        ]);
        $task->save();
    }

    public function create(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('create-users'))
                return response()->json([
                    'success' => false,
            ], 400);
            $user = new User([
                'userName' => strtolower($request->email),
                'password' => Hash::make('DefaultPass.7#')
            ]);
            $user->save();
            $role = Role::find(2);
            $user->assignRole($role->name);
            self::createUserProfile($request);
            self::createUserBalance($user->userId);
            self::createUserScore($user->userId);
            return response()->json(['success' => 'true'], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function read(){
        $user = AuthController::getUser();
        if(!$user->can('create-users'))
            return response()->json([
                'success' => false,
        ], 400);
        return response()->json(['response' => 'success'], 200);
    }

    public function update($email){
        try{
            $user = AuthController::getUser();
            if(!$user->can('update-users'))
                return response()->json([
                    'success' => false,
            ], 400);
            $userId = $request->userId;
            $user = User::all()->where('userId',$userId)->first();
            $user->userName = $request->userName;
            $user->password = $request->password;
            $user->save();
            return response()->json(['response' => 'success'], 200);
        }
        catch (Throwable $t){
            return $t;
        }
    }

    public function delete(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('delete-users'))
                return response()->json([
                    'success' => false,
            ], 400);
            $userId = $request->userId;
            $user = User::all()->where("userId",$userId)->first();
            $user->enabled = "0";
            $user->save();
            return response()->json(['response' => 'success'], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('userName', 'password');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        return response()->json(compact('token'));
    }
    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['user_not_found'], 404);
            }
            } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                    return response()->json(['token_expired'], $e->getStatusCode());
            } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                    return response()->json(['token_invalid'], $e->getStatusCode());
            } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
                    return response()->json(['token_absent'], $e->getStatusCode());
            }
            return response()->json(compact('user'));
    }

    public function quantity(){
        $count = User::select('*')
                        ->where('enabled','1')
                        ->get()
                        ->count();

        return $count;
    }
}
