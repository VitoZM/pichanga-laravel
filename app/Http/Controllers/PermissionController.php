<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PermissionController extends Controller
{
    public function read(){
        $permissions = DB::table('permissions')->get()->toJson();
        return $permissions;
    }
}
