<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Payment;
use App\Models\Task;

class PaymentController extends Controller
{
    public static function create(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('create-payments'))
                return response()->json([
                    'success' => false,
            ], 400);
            $payment = new Payment($request->all());
            $payment->save();
            $task = Task::find($payment->taskId);
            $task->status = "2";
            $task->save();
            return response()->json(['success' => true, 'payment' => $payment], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public static function payBalance($task){
        $paymentsSum = DB::table('payments')
                            ->where('taskId',$task->taskId)
                            ->sum('amount');
        $finalCost = $task->finalCost;
        $balance = $finalCost - $paymentsSum;
        if($balance > 0){
            $payment = new Payment([
                'amount' => $balance,
                'taskId' => $task->taskId
            ]);
            $payment->save();
        }
    }

    public static function createFirstPayment($initialPayment,$taskId){
        $status = '0';
        if($initialPayment == null)
            $initialPayment = 0;
        if($initialPayment > 0)
            $status = '2';
        $payment = new Payment([
            'amount' => $initialPayment,
            'taskId' => $taskId
        ]);
        $payment->save();
        return $status;
    }

    public static function updateFirstPayment($initialPayment,$taskId){
        $status = '0';
        if($initialPayment > 0)
            $status = '2';
        $payment = self::getFirstPayment($taskId);
        $payment->amount = $initialPayment;
        $payment->save();
        return $status;
    }

    public static function getFirstPayment($taskId){
        $payment = Payment::all()
                            ->where('taskId',$taskId)
                            ->first();
        return $payment;
    }

    public static function deletePaymentsExceptFirst(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('delete-payments'))
                return response()->json([
                    'success' => false,
            ], 400);
            $payment = Payment::all()
                                ->where('taskId',$request->taskId)
                                ->first();
            $payment->amount = 0;
            $payment->save();
            $payments = Payment::all()
                                ->where('taskId',$request->taskId)
                                ->where('amount','>',0);
            foreach($payments as $payment)
                $payment->delete();
            $task = Task::find($request->taskId);
            $task->status = '0';
            $task->save();
            return response()->json(['success' => true], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public static function deletePayments($taskId){
        try{
            $payments = Payment::all()->where('taskId',$taskId);
            foreach($payments as $payment)
                $payment->delete();
            return true;
        }
        catch (Throwable $t){
            return false;
        }
    }
}
