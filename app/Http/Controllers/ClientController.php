<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;

class ClientController extends Controller
{
    public function create(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('create-clients'))
                return response()->json([
                    'success' => false,
            ], 400);
            $client = new Client($request->all());
            $client->save();
            return response()->json(['success' => true, 'client' => $client], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function read(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('read-clients'))
                return response()->json([
                    'success' => false,
            ], 400);
            $clients = Client::select('*')->where('enabled','1')->get();
            return response()->json(['success' => true, 'clients' => $clients], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function update(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('read-clients'))
                return response()->json([
                    'success' => false,
            ], 400);
            $client = Client::find($request->clientId);
            $client->fill($request->all());
            $client->save();
            return response()->json(['success' => true, 'client' => $client], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function delete(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('read-clients'))
                return response()->json([
                    'success' => false,
            ], 400);
            $client = Client::find($request->clientId);
            $client->delete();
            return response()->json(['success' => true,], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }
}
