<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Message;

class MessageController extends Controller
{
    public function create(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('create-messages'))
                return response()->json([
                    'success' => false,
            ], 400);
            $message = new Message($request->all());
            $message->save();
            return response()->json(['success' => true, 'message' => $message], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function read(){
        try{
            $user = AuthController::getUser();
            if(!$user->can('read-messages'))
                return response()->json([
                    'success' => false,
            ], 400);
            $messages = Message::all();
            return response()->json([
                'sucess' => true,
                'messages' => $messages,
            ], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function update(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('update-messages'))
                return response()->json([
                    'success' => false,
            ], 400);
            $message = Message::find($request->messageId);
            $message->fill($request->all());
            $message->save();
            return response()->json([
                'sucess' => true,
                'message' => $message,
            ], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function delete(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('update-messages'))
                return response()->json([
                    'success' => false,
            ], 400);
            $message = Message::find($request->messageId);
            $message->delete();
            return response()->json([
                'sucess' => true,
                'message' => $message,
            ], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function findById(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('read-messages'))
                return response()->json([
                    'success' => false,
            ], 400);
            $message = Message::find($request->messageId);
            return response()->json([
                'sucess' => true,
                'message' => $message,
            ], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }
}
