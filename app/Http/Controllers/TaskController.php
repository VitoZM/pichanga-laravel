<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Task;
use App\Models\Payment;
use App\Models\Client;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\BalanceController;

class TaskController extends Controller
{
    public function create(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('create-tasks'))
                return response()->json([
                    'success' => false,
            ], 400);
            $task = new Task($request->all());
            if($request->newClient == "true"){
                $clientId = self::createClient($request->client,$request->phone);
                $task->clientId = $clientId;
            }
            $task->finalCost = $task->userCost + $task->gain;
            $task->receptionDate .= " ".$request->receptionDateTime;
            $task->deliveryDate .= " ".$request->deliveryDateTime;
            $initialPayment = $request->payment;
            $task->balance = $task->finalCost - $initialPayment;
            if($request->description == null)
                $task->description = "SIN OBSERVACIONES";
            $task->save();
            $task->status = PaymentController::createFirstPayment($initialPayment,$task->taskId);
            $task->save();
            return response()->json(['success' => true, 'task' => $task], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    private static function createClient($name,$phone){
        $client = new Client([
            'name' => $name,
            'phone' => $phone
        ]);
        $client->save();
        return $client->clientId;
    }

    public function read(){
        try{
            $user = AuthController::getUser();
            if(!$user->can('read-tasks'))
                return response()->json([
                    'success' => false,
            ], 400);
            $tasks = Task::join('profiles','profiles.userId','tasks.userId')
                        ->join('clients','clients.clientId','tasks.clientId')
                        ->select('profiles.name','profiles.lastName','profiles.city','profiles.photo','tasks.*','clients.name as client')
                        ->where('tasks.type','<>','9')
                        ->where('tasks.created_at', '>=', now()->subDays(30))
                        ->orderByDesc('tasks.taskId')
                        ->get();

            return response()->json([
                'sucess' => true,
                'tasks' => $tasks,
            ], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function update(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('update-tasks'))
                return response()->json([
                    'success' => false,
            ], 400);
            $task = Task::all()->where('taskId',$request->taskId)->first();
            $prevFinalCost = $task->finalCost;
            $task->fill($request->all());
            $costDifference = $task->userCost + $task->gain - $prevFinalCost;
            $task->finalCost = $task->userCost + $task->gain;
            $task->receptionDate .= " ".$request->receptionDateTime;
            $task->deliveryDate .= " ".$request->deliveryDateTime;
            $task->balance = $task->balance + $costDifference;
            /*$initialPayment = $request->payment;
            $task->balance = $task->finalCost - $initialPayment;
            $task->save();
            $task->status = PaymentController::updateFirstPayment($initialPayment,$task->taskId);*/
            $task->save();
            return response()->json(['success' => true, 'task' => $task], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function delete(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('delete-tasks'))
                return response()->json([
                    'success' => false,
            ], 400);
            $task = Task::find($request->taskId);
            if(!PaymentController::deletePayments($request->taskId))
                return response()->json([
                    'success' => false,
                ], 400);
            $task->delete();
            return response()->json(['success' => true], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function finish(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('update-tasks'))
                return response()->json([
                    'success' => false,
            ], 400);
            $task = Task::find($request->taskId);
            $task->status = '1';
            $task->score = $request->score;
            $task->balance = 0;
            PaymentController::payBalance($task);
            BalanceController::createBalance(
                    $task->userCost,
                    $task->title."(".$task->taskId.")",
                    $task->userId
                );
            $task->save();
            return response()->json(['success' => true], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }

    public function findById(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('read-tasks'))
                return response()->json([
                    'success' => false,
                ], 400);
            $task = Task::join('profiles','profiles.userId','tasks.userId')
                        ->join('clients','clients.clientId','tasks.clientId')
                        ->select('profiles.userId','profiles.name','profiles.lastName','profiles.photo','tasks.*','clients.name as client','clients.phone')
                        ->where('tasks.taskId',$request->taskId)
                        ->first();
            $payments = Payment::select('*')
                                ->where('taskId',$task->taskId)
                                ->orderByDesc('created_at')
                                ->get();
            $paymentsSum = DB::table('payments')
                                ->where('taskId',$request->taskId)
                                ->sum('amount');
            $task->balance = $task->finalCost - $paymentsSum;
            return response()->json([
                'sucess' => true,
                'task' => $task,
                'payments' => $payments
            ], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }
//falta poner un enlace en las tareas del tablero kanban directo para que se pueda ver la tarea...
    public function getTitleAndPayments(Request $request){
        try{
            $user = AuthController::getUser();
            if(!$user->can('read-tasks'))
                return response()->json([
                    'success' => false,
            ], 400);
            $task = Task::find($request->taskId);
            $payments = Payment::select('*')
                                ->where('taskId',$task->taskId)
                                ->orderByDesc('created_at')
                                ->get();
            $paymentsSum = DB::table('payments')
                                ->where('taskId',$request->taskId)
                                ->sum('amount');
            $task->balance = $task->finalCost - $paymentsSum;
            return response()->json([
                'sucess' => true,
                'task' => $task,
                'payments' => $payments
            ], 200);
        }
        catch (Throwable $t){
            return response()->json(['response' => $t], 500);
        }
    }
}
