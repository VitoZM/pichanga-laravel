<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'dashboard', 'guard_name' => 'web']);

        Permission::create(['name' => 'create-logs', 'guard_name' => 'web']);
        Permission::create(['name' => 'update-logs', 'guard_name' => 'web']);
        Permission::create(['name' => 'read-logs', 'guard_name' => 'web']);

        Permission::create(['name' => 'users-module', 'guard_name' => 'web']);
        Permission::create(['name' => 'create-users', 'guard_name' => 'web']);
        Permission::create(['name' => 'read-users', 'guard_name' => 'web']);
        Permission::create(['name' => 'update-users', 'guard_name' => 'web']);
        Permission::create(['name' => 'delete-users', 'guard_name' => 'web']);

        Permission::create(['name' => 'tasks-module', 'guard_name' => 'web']);
        Permission::create(['name' => 'create-tasks', 'guard_name' => 'web']);
        Permission::create(['name' => 'read-tasks', 'guard_name' => 'web']);
        Permission::create(['name' => 'update-tasks', 'guard_name' => 'web']);
        Permission::create(['name' => 'delete-tasks', 'guard_name' => 'web']);

        Permission::create(['name' => 'balances-module', 'guard_name' => 'web']);
        Permission::create(['name' => 'create-balances', 'guard_name' => 'web']);
        Permission::create(['name' => 'read-balances', 'guard_name' => 'web']);
        Permission::create(['name' => 'update-balances', 'guard_name' => 'web']);
        Permission::create(['name' => 'delete-balances', 'guard_name' => 'web']);

        Permission::create(['name' => 'payments-module', 'guard_name' => 'web']);
        Permission::create(['name' => 'create-payments', 'guard_name' => 'web']);
        Permission::create(['name' => 'read-payments', 'guard_name' => 'web']);
        Permission::create(['name' => 'update-payments', 'guard_name' => 'web']);
        Permission::create(['name' => 'delete-payments', 'guard_name' => 'web']);

        Permission::create(['name' => 'messages-module', 'guard_name' => 'web']);
        Permission::create(['name' => 'create-messages', 'guard_name' => 'web']);
        Permission::create(['name' => 'read-messages', 'guard_name' => 'web']);
        Permission::create(['name' => 'update-messages', 'guard_name' => 'web']);
        Permission::create(['name' => 'delete-messages', 'guard_name' => 'web']);

        Permission::create(['name' => 'clients-module', 'guard_name' => 'web']);
        Permission::create(['name' => 'create-clients', 'guard_name' => 'web']);
        Permission::create(['name' => 'read-clients', 'guard_name' => 'web']);
        Permission::create(['name' => 'update-clients', 'guard_name' => 'web']);
        Permission::create(['name' => 'delete-clients', 'guard_name' => 'web']);
    }
}
