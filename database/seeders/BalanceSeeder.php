<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BalanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('balances')->insert([
            'amount' => 0,
            'description' => 'Apertura de cuenta',
            'userId' => 1
        ]);

        DB::table('balances')->insert([
            'amount' => 0,
            'description' => 'Apertura de cuenta',
            'userId' => 2
        ]);
    }
}
