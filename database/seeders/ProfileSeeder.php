<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert([
            'userId' => 1,
            'name' => 'Alvaro Luis',
            'lastName' => 'Zapata Moscoso',
            'ci' => '10331470',
            'issued' => 'CH.',
            'city' => 'Sucre',
            'phone' => '79301442',
            'email' => 'v170zam@gmail.com',
            'photo' => '1.jpg',
            'bankAccount' => '4500627927',
            'bank' => 'BNB',
            'knowledges' => '["Programación","Desarrollo Web"]',
            'created_at' => '2020/11/21 08:00:00',
            'updated_at' => '2020/11/21 08:00:00'
        ]);

        DB::table('profiles')->insert([
            'userId' => 2,
            'name' => 'Andres',
            'lastName' => 'Gutierrez',
            'ci' => '-',
            'issued' => 'CB.',
            'city' => 'Cochabamba',
            'phone' => '75966533',
            'email' => 'pichanga.bol@gmail.com',
            'photo' => '2.jpg',
            'bankAccount' => '75966533',
            'bank' => 'TIGO MONEY',
            'knowledges' => '["DE TODO"]',
            'created_at' => '2020/11/21 08:00:00',
            'updated_at' => '2020/11/21 08:00:00'
        ]);
    }
}
