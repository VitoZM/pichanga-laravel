<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payments')->insert([
            'paymentId' => 1,
            'amount' => 10,
            'taskId' => 1
        ]);

        DB::table('payments')->insert([
            'paymentId' => 1,
            'amount' => 10,
            'taskId' => 2
        ]);
    }
}
