<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'userId' => 1,
            'userName' => 'v170zam@gmail.com',
            'password' => Hash::make('SwordPichanga5.'),
            'created_at' => '2020/11/21 08:00:00',
            'updated_at' => '2020/11/21 08:00:00'
        ]);

        DB::table('users')->insert([
            'userId' => 2,
            'userName' => 'pichanga.bol@gmail.com',
            'password' => Hash::make('PichangaKing7#'),
            'created_at' => '2020/11/21 08:00:00',
            'updated_at' => '2020/11/21 08:00:00'
        ]);
    }
}
