<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert([
            'taskId' => 1,
            'title' => 'Apertura de cuenta',
            'userCost' => 0,
            'gain' => 0,
            'finalCost' => 0,
            'receptionDate' => '2020/12/31 08:00:00',
            'deliveryDate' => '2021/01/31 08:00:00',
            'type' => '9',
            'subject' => '-',
            'score' => 0,
            'status' => 1,
            'balance' => 0,
            'description' => '-',
            'userId' => 1,
            'clientId' => 1
        ]);

        DB::table('tasks')->insert([
            'taskId' => 2,
            'title' => 'Apertura de cuenta',
            'userCost' => 0,
            'gain' => 0,
            'finalCost' => 0,
            'receptionDate' => '2020/12/31 08:00:00',
            'deliveryDate' => '2021/01/31 08:00:00',
            'type' => '9',
            'subject' => '-',
            'score' => 0,
            'status' => 1,
            'balance' => 0,
            'description' => '-',
            'userId' => 2,
            'clientId' => 1
        ]);
    }
}
