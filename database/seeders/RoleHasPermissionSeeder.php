<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

class RoleHasPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::find(1);

        $permissions = DB::table('permissions')->get();

        foreach($permissions as $permission)
            $role->givePermissionTo($permission->name);

        $role = Role::find(2);
        $role->givePermissionTo('dashboard');

        $role->givePermissionTo('users-module');
        $role->givePermissionTo('update-users');
    }
}
