<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('taskId');
            $table->string('title','100');
            $table->decimal('userCost', $precision = 11, $scale = 2);
            $table->decimal('gain', $precision = 11, $scale = 2);
            $table->decimal('finalCost', $precision = 11, $scale = 2);
            $table->decimal('balance', $precision = 11, $scale = 2);
            $table->dateTime('receptionDate');
            $table->dateTime('deliveryDate');
            $table->string('type','1');
            $table->string('subject','60');
            $table->decimal('score', $precision = 11, $scale = 2)->default(0);
            $table->text('description')->default("Sin Observaciones");
            $table->string('status','1')->default('0');
            $table->string('enabled','1')->default('1');
            $table->unsignedInteger('userId');
            $table->foreign('userId')->references('userId')->on('users');
            $table->unsignedInteger('clientId');
            $table->foreign('clientId')->references('clientId')->on('clients');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
