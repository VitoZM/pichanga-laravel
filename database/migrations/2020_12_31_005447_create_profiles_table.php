<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->unsignedInteger('userId');
            $table->string('name',30);
            $table->string('lastName',30);
            $table->string('ci',30);
            $table->string('issued',3)->default('CB.');
            $table->string('city',90);
            $table->string('phone',20);
            $table->string('email',30)->unique();
            $table->string('photo','20')->default('default.jpg');
            $table->string('bankAccount','30');
            $table->string('bank','30');
            $table->text('knowledges');
            $table->timestamps();
            $table->primary('userId');
            $table->foreign('userId')->references('userId')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
